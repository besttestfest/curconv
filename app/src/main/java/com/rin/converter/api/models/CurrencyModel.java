package com.rin.converter.api.models;

/**
 * Created by Rin on 2017-06-18.
 */

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;


@Root(name = "Valute")
public class CurrencyModel {
    public static final String RUB = "RUB";
    public static final String USD = "USD";
    @Attribute(name = "ID")
    private String mId;
    @Element(name = "NumCode", required = true)
    private String mNumCode;

    @Element(name = "CharCode", required = true)
    private String mCharCode;

    @Element(name = "Nominal", required = true)
    private long mNominal;

    @Element(name = "Name", required = true)
    private String mName;

    @Element(name = "Value", required = true)
    private String mValue;

    public String getValue() {
        return mValue;
    }

    public String getCharCode() {
        return mCharCode;
    }

    public long getNominal() {
        return mNominal;
    }
}
