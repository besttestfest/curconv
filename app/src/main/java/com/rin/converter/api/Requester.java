package com.rin.converter.api;

import com.rin.converter.api.models.CurrenciesModel;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Requester {
    public static String makeRequest(String string) throws Exception {
        URL url = new URL(string);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        String result;
        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            result = readStream(in);
        } finally {
            urlConnection.disconnect();
        }
        return result;
    }

    private static String readStream(InputStream in) throws Exception {
        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line).append('\n');
        }
        return total.toString();
    }

    public interface OnUpdateListener {
        void onRequest();

        void onUpdate(CurrenciesModel model);

        void onError(Exception e);
    }
}
