package com.rin.converter.api.models;

/**
 * Created by Rin on 2017-06-18.
 */

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;


@Root(name = "ValCurs")
public class CurrenciesModel {
    @Attribute(name = "Date", required = false)
    private String data;
    @Attribute(name = "name", required = false)
    private String name;
    @ElementList(required = true, inline = true)
    private List<CurrencyModel> list = new ArrayList<>();

    public List<CurrencyModel> getList() {
        return this.list;
    }
}
