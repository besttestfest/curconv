package com.rin.converter.widgets;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.rin.converter.R;
import com.rin.converter.api.models.CurrencyModel;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rin on 2017-06-18.
 */

public class AmountView extends LinearLayout {
    private EditText mDecimalEditText;
    private Spinner mCurrencySpinner;
    private SimpleOnTextChangeListener mOnTextChangedListener;
    private SimpleOnCurrencyChangeListener mOnCurrencyChangedListener;
    private List<String> mList = new ArrayList<>();
    private ArrayAdapter<String> mSpinnerArrayAdapter;
    private List<CurrencyModel> mCurrenciesModel;
    private String mDefaultCurrency;

    private static final DecimalFormat UI_NUMBER_FORMAT = new DecimalFormat();

    static {
        Currency currency = Currency.getInstance(Locale.getDefault());
        UI_NUMBER_FORMAT.setMaximumFractionDigits(2);
        UI_NUMBER_FORMAT.setCurrency(currency);
    }


    public AmountView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public AmountView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AmountView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        LinearLayout amountLayout = (LinearLayout) layoutInflater.inflate(R.layout.currency_layout, this);

        mDecimalEditText = (EditText) amountLayout.findViewById(R.id.decimalEditText);
        mCurrencySpinner = (Spinner) amountLayout.findViewById(R.id.currencySpinner);
        initViewsLogic(mDecimalEditText, mCurrencySpinner);
    }

    private void initViewsLogic(EditText editText, Spinner spinner) {
        initEditText(editText);
        initSpinner(spinner);
    }

    private void initSpinner(Spinner spinner) {
        mSpinnerArrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, mList);
        mSpinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(mSpinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mOnCurrencyChangedListener != null) {
                    mOnCurrencyChangedListener.onCurrencyChanged(mList.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initEditText(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editText.removeTextChangedListener(this);
                String formatted = formatValue(s.toString());
                editText.setText(formatted);
                editText.setSelection(formatted.length());
                editText.addTextChangedListener(this);
                if (mOnTextChangedListener != null) {
                    mOnTextChangedListener.onTextChanged(s.toString());
                }
            }
            private String formatValue(String value) {
                String result;
                try {
                    result = UI_NUMBER_FORMAT.format(
                            Double.parseDouble(value.replaceAll("[,\\s]", ""))); //TODO Remove replace workaround on correct parsing
                } catch (Exception e) {
                    result = "";
                }
                if (BigDecimal.ZERO.equals(value)) {
                    result = "";
                }
                return result;
            }

        });
    }

    @NonNull
    public BigDecimal getAmount() {
        try {
            String source = mDecimalEditText.getText().toString().replaceAll("[,\\s]", "");
            Log.i(this.getClass().toString(), source);
            return new BigDecimal(UI_NUMBER_FORMAT.parse(source).doubleValue());
        } catch (ParseException e) {
            e.printStackTrace();
            return BigDecimal.ZERO;
        }
    }

    public void setOnTextChangedListener(SimpleOnTextChangeListener onTextChangedListener) {
        this.mOnTextChangedListener = onTextChangedListener;
    }

    public EditText getDecimalEditText() {
        return mDecimalEditText;
    }

    public Spinner getCurrencySpinner() {
        return mCurrencySpinner;
    }


    public void setCurrencies(List<CurrencyModel> currencyModels) {
        boolean needLazySetupOfDefValue = false;
        if (mCurrenciesModel == null || mCurrenciesModel.isEmpty()) {
            needLazySetupOfDefValue = true;
        }
        mCurrenciesModel = currencyModels;
        mList.clear();
        if (currencyModels != null) {
            for (CurrencyModel currency : currencyModels) {
                mList.add(currency.getCharCode());
            }
            if (!mList.contains(CurrencyModel.RUB)) {
                mList.add(CurrencyModel.RUB);
            }
        }
        mSpinnerArrayAdapter.notifyDataSetChanged();
        if (needLazySetupOfDefValue) {
            setCurrency(mDefaultCurrency);
        }
    }

    public String getCurrency() {
        if (mList != null && !mList.isEmpty()) {
            int index = mCurrencySpinner.getSelectedItemPosition();
            if (index < 0)
                index = 0;
            return mList.get(index);
        }
        return CurrencyModel.RUB;
    }

    public void setCurrency(String string) {
        int index = 0;
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).equalsIgnoreCase(string)) {
                index = i;
            }
        }
        mCurrencySpinner.setSelection(index);
    }

    public void setOnCurrencyChangedListener(SimpleOnCurrencyChangeListener onCurrencyChangedListener) {
        mOnCurrencyChangedListener = onCurrencyChangedListener;
    }

    public List<CurrencyModel> getCurrenciesModel() {
        return mCurrenciesModel;
    }

    public void setDefaultCurrency(String defaultCurrency) {
        mDefaultCurrency = defaultCurrency;
    }

    public interface SimpleOnTextChangeListener {
        void onTextChanged(String s);
    }

    public interface SimpleOnCurrencyChangeListener {
        void onCurrencyChanged(String s);
    }
}
