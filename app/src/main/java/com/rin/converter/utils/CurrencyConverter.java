package com.rin.converter.utils;

import com.rin.converter.widgets.AmountView;
import com.rin.converter.api.models.CurrencyModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Rin on 2017-06-18.
 */

public class CurrencyConverter {
    private static final NumberFormat API_NUMBER_FORMAT = NumberFormat.getInstance(Locale.FRANCE); // "FRANCE" because of "," separator in number. Who made this?! :)

    public static void performConvert(AmountView source, AmountView result) {
        BigDecimal value = prepareAndConvert(source, result);
        String string = value.setScale(2, RoundingMode.DOWN).toEngineeringString();
        result.setTag(string);
        result.getDecimalEditText().setText(string);
    }

    private static BigDecimal prepareAndConvert(AmountView source, AmountView result) {
        BigDecimal sourceDecimal = source.getAmount();
        String sourceCurrency = source.getCurrency();
        String resultCurrency = result.getCurrency();
        return convert(sourceDecimal, sourceCurrency, resultCurrency, result.getCurrenciesModel());
    }

    private static BigDecimal convert(BigDecimal sourceDecimal, String sourceCurrency, String resultCurrency, List<CurrencyModel> currenciesModel) {
        return sourceDecimal.multiply(getRate(sourceCurrency, resultCurrency, currenciesModel));
    }

    private static BigDecimal getRate(String resultCurrency, String sourceCurrency, List<CurrencyModel> currenciesModel) { //TODO Simplify
        BigDecimal number = BigDecimal.valueOf(1);
        if (currenciesModel != null) {
            for (CurrencyModel currencyModel : currenciesModel) {
                if (currencyModel.getCharCode().equals(resultCurrency)) {
                    try {
                        number = BigDecimal.valueOf(API_NUMBER_FORMAT.parse(currencyModel.getValue()).doubleValue());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return number.divide(BigDecimal.valueOf(currencyModel.getNominal()), 2, BigDecimal.ROUND_DOWN);
                } else if (currencyModel.getCharCode().equals(sourceCurrency)) {
                    try {
                        number = BigDecimal.valueOf(API_NUMBER_FORMAT.parse(currencyModel.getValue()).doubleValue());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    BigDecimal temp = BigDecimal.valueOf(1)
                            .divide(number, 10, BigDecimal.ROUND_DOWN);
                    return temp
                            .divide(BigDecimal.valueOf(currencyModel.getNominal()), 10, BigDecimal.ROUND_DOWN);
                }
            }
        }
        return number;
    }
}
