package com.rin.converter.utils;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.rin.converter.BuildConfig;
import com.rin.converter.api.Requester;
import com.rin.converter.api.models.CurrenciesModel;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class DataFetcherUtil {
    private static final Serializer PERSISTER = new Persister();
    private static final String FILE_NAME = "currency_cache.xml";

    public static void getCurrencyModel(final Context context, final Requester.OnUpdateListener listener) {
        final Handler hostThreadHandler = new Handler();
        new Thread(new Runnable() { //TODO Replace thread on Loader for correct Activity Lifecycle handle.
            @Override
            public void run() {
                getStoredCurrenciesModel();
                tryToUpdateStoredModel();
            }

            private void tryToUpdateStoredModel() {
                try {
                    final CurrenciesModel model = updateCache();
                    hostThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onUpdate(model);
                        }
                    });
                } catch (final Exception e) {
                    hostThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onError(e);
                        }
                    });
                }
            }

            private void getStoredCurrenciesModel() {
                CurrenciesModel model = null;
                try {
                    model = restoreCurrencyModel(context);
                    final CurrenciesModel finalModel = model;
                    hostThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onUpdate(finalModel);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (model == null || model.getList().isEmpty()) {
                    hostThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onRequest();
                        }
                    });
                }
            }

            private CurrenciesModel updateCache() throws Exception {
                String result = Requester.makeRequest(BuildConfig.CURRENCY_URL);
                saveToFile(context, result);
                return PERSISTER.read(CurrenciesModel.class, result);
            }

        }).start();
    }

    private static CurrenciesModel restoreCurrencyModel(Context context) throws Exception {
        String result = readFromFile(context);
        return PERSISTER.read(CurrenciesModel.class, result);
    }

    @NonNull
    private static String readFromFile(Context context) throws IOException {
        final BufferedReader in = new BufferedReader(new InputStreamReader(context.openFileInput(FILE_NAME)));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = in.readLine()) != null) {
            total.append(line).append('\n');
        }
        return total.toString();
    }

    private static void saveToFile(Context context, String result) throws IOException {
        final BufferedWriter out = new BufferedWriter(
                new OutputStreamWriter(context.openFileOutput(FILE_NAME, 1024)));
        out.write(result);
        out.close();
    }

}
