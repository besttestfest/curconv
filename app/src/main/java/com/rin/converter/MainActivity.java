package com.rin.converter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Toast;

import com.rin.converter.api.Requester.OnUpdateListener;
import com.rin.converter.api.models.CurrenciesModel;
import com.rin.converter.api.models.CurrencyModel;
import com.rin.converter.utils.CurrencyConverter;
import com.rin.converter.utils.DataFetcherUtil;
import com.rin.converter.widgets.AmountView;

public class MainActivity extends Activity {

    private ProgressDialog mProgressDialog;
    private AmountView mSourceAmountView;
    private AmountView mResultAmountView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initProgressBar();
        initViews();
        invalidateData();
    }

    private void initViews() {
        mSourceAmountView = (AmountView) findViewById(R.id.sourceLayout);
        mSourceAmountView.setDefaultCurrency(CurrencyModel.RUB);
        mSourceAmountView.getCurrencySpinner().setEnabled(false); //Because we have only RUB related rates.

        mResultAmountView = (AmountView) findViewById(R.id.resultLayout);
        mResultAmountView.setDefaultCurrency(CurrencyModel.USD);

        initConverter(mSourceAmountView, mResultAmountView);
        initConverter(mResultAmountView, mSourceAmountView);
    }


    private void initConverter(final AmountView source, final AmountView result) {
        source.setOnTextChangedListener(new AmountView.SimpleOnTextChangeListener() {
            @Override
            public void onTextChanged(String s) {
                if (!s.equals(source.getTag())) {
                    CurrencyConverter.performConvert(source, result);
                }
            }
        });
        source.setOnCurrencyChangedListener(new AmountView.SimpleOnCurrencyChangeListener() {
            @Override
            public void onCurrencyChanged(String s) {
                CurrencyConverter.performConvert(source, result);
            }
        });
    }


    private void initProgressBar() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle(getString(R.string.first_progress_title));
        mProgressDialog.setMessage(getString(R.string.first_progress_message));
    }

    private void invalidateData() {
        DataFetcherUtil.getCurrencyModel(this, new OnUpdateListener() {
            @Override
            public void onRequest() {
                mProgressDialog.show();
            }

            @Override
            public void onUpdate(CurrenciesModel model) {
                mSourceAmountView.setCurrencies(model.getList());
                mResultAmountView.setCurrencies(model.getList());

                mProgressDialog.dismiss();
            }

            @Override
            public void onError(Exception e) {
                Toast.makeText(
                        getApplicationContext(),
                        R.string.toast_update_currencies_error,
                        Toast.LENGTH_LONG)
                        .show();
                mProgressDialog.dismiss();
            }
        });
    }
}
